#ifndef LPP_PARSE_HPP__
#define LPP_PARSE_HPP__

#include<string>
#include<sstream>
#include<vector>
#include<queue>
#include<functional>

#include "lex.hpp"


namespace lpp{namespace parse{

using namespace lex;


class parse_error: std::exception
{
    public:
        parse_error(std::string msg): m{msg} {}
        const char* what() { return m.c_str(); }
    private:
        std::string m;
};

std::string expected_token(LuaToken tok, std::string exp)
{
    std::stringstream err;
    err << "Expected `" << exp << "` near " << tok.raw;
    return err.str();
}

enum class ASTNodeType
{
    empty, literal, id, binop, unop, funcdef, funcbody, funccall,
    var, localvar, program, forloop, forinitializer, cond, condpart,
    ret
};


struct ASTNode
{
    ASTNodeType type;
    LuaToken token;
    std::vector<ASTNode> params;

    ASTNode(ASTNodeType tp, LuaToken tk): type{tp}, token{tk} {}


    void show(int ident=0)
    {
        std::string t = type == ASTNodeType::literal ? "Literal":
            type == ASTNodeType::id ? "id" :
            type == ASTNodeType::binop ? "binop" : 
            type == ASTNodeType::unop ? "unop" :
            type == ASTNodeType::funcdef ? "funcdef" :
            type == ASTNodeType::funcbody ? "funcbody" :
            type == ASTNodeType::funccall ? "funccall" :
            type == ASTNodeType::var ? "var" :
            type == ASTNodeType::localvar ? "localvar" :
            type == ASTNodeType::program ? "program" :
            type == ASTNodeType::forloop ? "forloop" :
            type == ASTNodeType::forinitializer ? "forinitializer" :
            type == ASTNodeType::cond ? "cond" :
            type == ASTNodeType::condpart ? "condpart" :
            type == ASTNodeType::ret ? "return" :
            type == ASTNodeType::empty ? "Empty" : "unk";
        for(int i = 0; i<ident; i++)std::cout<<" ";
        std::cout << "("<<t<<")\t" << token.raw << "\n";
        for(auto x: params) x.show(ident+4);
    }
};


class Parser
{
    public:
        Parser() = default;
        Parser(const Parser&) = default;
        Parser(std::vector<LuaToken> t): tokens{t}, tc{0} {}

        ASTNode parse_paren();
        ASTNode parse_literal();
        ASTNode parse_identifier();
        ASTNode parse_binop(std::string oper);
        ASTNode parse_unop();
        ASTNode parse_exp();
        ASTNode parse_funcdef();
        ASTNode parse_funccall(bool as_statement=true);
        ASTNode parse_statement();
        ASTNode parse_assignment();
        ASTNode parse_program();
        ASTNode parse_for();
        ASTNode parse_if();
        ASTNode parse_return();

        bool parse_elseif(ASTNode& cond);

        bool is_end() const{ return tc >= (tokens.size() - 1); }

    private:
        std::vector<LuaToken> tokens;
        std::size_t tc;

        LuaToken peek() const{ return tokens[tc]; }
        LuaToken prev() const{ return tokens[tc-1]; }
        LuaToken next() const{ return tokens[tc+1]; }
        LuaToken consume()
        {
            if(tc >= tokens.size() - 1)
            {
                return peek();
            }
            return tokens[tc++];
        }
};


ASTNode Parser::parse_paren()
{
    if(!is_end() && peek().type != LuaTokenType::lparen)
        return ASTNode(ASTNodeType::empty, LuaToken());
    consume();
    auto next_exp = parse_exp();
    auto closing = consume();
    if(closing.type != LuaTokenType::rparen)
    {
        std::stringstream err;
        err << "Unclosed parentheses at " << closing.row << "," << closing.col;
        throw parse_error(err.str());
    }
    return next_exp;
}

bool Parser::parse_elseif(ASTNode& cond)
{
    if(peek().raw != "elseif")
        return false;
    ASTNode elseifcond(ASTNodeType::condpart, consume());
    elseifcond.params.push_back(parse_exp());
    if(consume().raw != "then")
        throw parse_error(expected_token(prev(), "then"));
    while(peek().raw != "elseif" && peek().raw != "else" && peek().raw != "end")
    {
        elseifcond.params.push_back(parse_statement());
    }
    cond.params.push_back(elseifcond);
    return parse_elseif(cond);
}

ASTNode Parser::parse_return()
{
    if(peek().type != LuaTokenType::keyword && peek().raw != "return")
        throw parse_error("Expecting statement");
    ASTNode ret(ASTNodeType::ret, consume());
    if(peek().raw != "end")
        ret.params.push_back(parse_exp());
    return ret;
}

ASTNode Parser::parse_if()
{
    auto token = peek();
    if(token.type != LuaTokenType::keyword || token.raw != "if")
        return parse_return();
    ASTNode cond(ASTNodeType::cond, LuaToken());
    ASTNode ifcond(ASTNodeType::condpart, consume());
    ifcond.params.push_back(parse_exp());
    token = consume(); // then
    if(token.raw != "then")
        throw parse_error(expected_token(token, "then"));
    while(peek().raw != "elseif" && peek().raw != "else" && peek().raw != "end")
    {
        ifcond.params.push_back(parse_statement());
    }
    cond.params.push_back(ifcond);
    while(parse_elseif(cond));
    if(peek().raw == "else")
    {
        ASTNode elsecond(ASTNodeType::condpart, consume());
        while(peek().raw != "end")
            elsecond.params.push_back(parse_statement());
        cond.params.push_back(elsecond);
    }
    consume(); // end
    return cond;
}

ASTNode Parser::parse_for()
{
    if(!(peek().type == LuaTokenType::keyword && peek().raw == "for"))
        return parse_if();
    ASTNode floop(ASTNodeType::forloop, consume());
    ASTNode floopinit(ASTNodeType::forinitializer, peek());
    if(peek().type != LuaTokenType::id)
        throw parse_error(expected_token(peek(), "identifier"));
    auto loopvar = parse_identifier();
    if(peek().raw != "=")
        throw parse_error(expected_token(peek(), "="));
    consume(); // = 
    floopinit.params.push_back(loopvar);
    floopinit.params.push_back(parse_exp());
    if(peek().raw != ",")
        throw parse_error(expected_token(peek(), ","));
    consume(); // ,
    floopinit.params.push_back(parse_exp());
    auto token = consume();
    if(token.raw == ",")
    {
        floopinit.params.push_back(parse_exp());
        token = consume(); // do
    }
    if(token.raw != "do")
        throw parse_error(expected_token(token, "do"));
    floop.params.push_back(floopinit);
    while(peek().raw != "end")
        floop.params.push_back(parse_statement());
    consume(); // end
    return floop;
}

ASTNode Parser::parse_assignment()
{
    if(peek().raw != "local" && peek().type != LuaTokenType::id)
        return parse_for();
    auto token = consume(); // local or id
    bool local = (token.type == LuaTokenType::keyword && token.raw == "local");
    if(local) token = consume(); // id
    if(token.type != LuaTokenType::id)
        throw parse_error(expected_token(token, "identifier"));
    ASTNode ass(local ? ASTNodeType::localvar : ASTNodeType::var, token);
    if(!local && peek().raw != "=")
        throw parse_error(expected_token(token, "= (assignment"));
    if(peek().raw == "=")
    {
        token = consume(); // =
        ass.params.push_back(parse_exp());
    }
    return ass;
}

ASTNode Parser::parse_funccall(bool as_statement)
{
    auto descent = [this, as_statement] ()
    {
        return as_statement ? parse_assignment() : parse_identifier();
    };
    ASTNode call(ASTNodeType::funccall, LuaToken());
    if(peek().type == LuaTokenType::id)
    {
        if(next().type != LuaTokenType::lparen) return descent();
        call.params.push_back(parse_identifier());
        consume(); // (
    }
    else if(peek().type == LuaTokenType::lparen && next().raw == "function")
    {
        consume(); // (
        call.params.push_back(parse_funcdef());
        consume(); // )
        consume(); // (
    }
    else
    {
        return descent();
    }
    int idx = 0;
    while(peek().type != LuaTokenType::rparen)
    {
        if(idx++ != 0)
        {
            if(peek().type != LuaTokenType::comma)
                throw parse_error(expected_token(peek(), ","));
            consume(); // ,
        }
        call.params.push_back(parse_exp());
    }
    consume(); // )
    return call;
}

ASTNode Parser::parse_statement()
{
   return parse_funccall();
}

ASTNode Parser::parse_funcdef()
{
    auto token = peek();
    if(!(token.type == LuaTokenType::keyword && token.raw == "function"))
        return parse_paren();
    if(next().type != LuaTokenType::lparen)
    {
        throw parse_error(expected_token(token, "("));
    }
    consume(); // function
    consume(); // (
    ASTNode f(ASTNodeType::funcdef, LuaToken());
    int idx = 0;
    while(peek().type != LuaTokenType::rparen)
    {
        if(idx++ != 0)
        {
            if(peek().type != LuaTokenType::comma)
                throw parse_error(expected_token(peek(), ","));
            consume(); // ,
        }
        if(peek().type != LuaTokenType::id)
            throw parse_error(expected_token(peek(), "identifier"));
        f.params.push_back(parse_identifier());
    }
    consume(); // )
    ASTNode fb(ASTNodeType::funcbody, LuaToken());
    while(peek().raw != "end")
    {
        fb.params.push_back(parse_statement());
    }
    consume(); // end
    f.params.push_back(fb);
    return f;
}

ASTNode Parser::parse_literal()
{
    auto token = peek();
    if(token.type == LuaTokenType::string||
       token.type == LuaTokenType::number||
       token.type == LuaTokenType::nil||
       token.type == LuaTokenType::boolean)
            return ASTNode(ASTNodeType::literal, consume());
    return parse_funcdef();
}

ASTNode Parser::parse_identifier()
{
    if(peek().type == LuaTokenType::id)
        return ASTNode(ASTNodeType::id, consume());
    return parse_literal();
}

ASTNode Parser::parse_unop()
{
    if(std::find(UNOPS.begin(), UNOPS.end(), peek().raw) == UNOPS.end())
        return parse_funccall(false);
    ASTNode exp(ASTNodeType::unop, consume());
    exp.params.push_back(parse_exp());
    return exp;
}

ASTNode Parser::parse_binop(std::string oper)
{
    auto op_idx = std::find(OPERATORS.begin(), OPERATORS.end(), oper);
    auto higher = (op_idx == OPERATORS.end() || op_idx == (OPERATORS.end() -1))
        ? parse_unop()
        : parse_binop(*std::next(op_idx, 1));
    if(peek().raw != oper) return higher;
    ASTNode exp(ASTNodeType::binop, consume());
    exp.params.push_back(higher);
    exp.params.push_back(parse_binop(oper));
    return exp;
}

ASTNode Parser::parse_exp() { return parse_binop(*OPERATORS.begin()); }

ASTNode Parser::parse_program()
{
    ASTNode program(ASTNodeType::program, LuaToken());
    while(!is_end())
    {
        program.params.push_back(parse_statement());
    }
    return program;
}

}}


#endif
