#ifndef LPP_CODEGEN_HPP__
#define LPP_CODEGEN_HPP__

#include<string>
#include<fstream>
#include<cassert>

#include "parse.hpp"

namespace lpp{ namespace codegen{

using namespace lpp::parse;


class Generator
{
    public:
        Generator() = delete;
        Generator(Generator&) = delete;
        Generator(Generator&&) = delete;
        Generator(std::string fname): output{fname}
        {
            outfile.open(fname);
            outfile << "#include\"lpp.hpp\"\n";
            outfile << "using namespace lpp;\n";
            outfile << "auto _gcntx = Context();\n";
            emit_scope();
            outfile << "int main(){\n";
            outfile << "load_stdlib(scope);\n";
        }
        ~Generator()
        {
            outfile << "\nreturn 0;\n";
            outfile << "}\n";
            outfile.close();
        }

        void generate(ASTNode program);

    private:
        std::string output;
        std::ofstream outfile;

        void emit_scope();
        void emit_assign(std::string, ASTNode, bool);

        void emit_statement(ASTNode);
        void emit_funccall(ASTNode);
        void emit_assignment(ASTNode);
        void emit_return(ASTNode);
        void emit_forloop(ASTNode);
        void emit_condition(ASTNode);

        void emit_expr(ASTNode);
        void emit_literal(ASTNode);
        void emit_identifier(ASTNode);
        void emit_funcdef(ASTNode);
        void emit_binop(ASTNode);
        void emit_unop(ASTNode);
};


void Generator::generate(ASTNode program)
{
    for(auto statement: program.params)
    {
        emit_statement(statement);
    }
}

void Generator::emit_scope()
{
    outfile << "Scope scope(_gcntx);\n";
}

void Generator::emit_assign(std::string id, ASTNode expr, bool local=true)
{
    outfile << "scope.set(\"" << id << "\", ";
    emit_expr(expr);
    outfile << ");";
}

void Generator::emit_funccall(ASTNode func)
{
    outfile << "std::dynamic_pointer_cast<Function>(";
    auto fun = func.params[0];
    assert((fun.type == ASTNodeType::id) || (fun.type == ASTNodeType::funcdef));
    if(fun.type == ASTNodeType::id)
        emit_identifier(fun);
    else
        emit_funcdef(fun);
    outfile << ")->call({";
    bool first = true;
    for(auto param = func.params.begin()+1; param != func.params.end(); param++)
    {
        if(!first) outfile << ",";
        emit_expr(*param);
        first = false;
    }
    outfile << "})";
}

void Generator::emit_statement(ASTNode statement)
{
    switch(statement.type)
    {
        case ASTNodeType::var:
        case ASTNodeType::localvar:
            emit_assignment(statement); break;
        case ASTNodeType::ret:
            emit_return(statement); break;
        case ASTNodeType::funccall:
            emit_funccall(statement); outfile << ";"; break;
        case ASTNodeType::forloop:
            emit_forloop(statement); break;
        case ASTNodeType::cond:
            emit_condition(statement); break;
        default: throw parse_error("Invalid statement");
    }
    outfile << "\n";
}

void Generator::emit_assignment(ASTNode assignment)
{
    std::string local = assignment.type == ASTNodeType::localvar ? "true" : "false";
    outfile << "scope.set(\"" << assignment.token.raw << "\", ";
    if(assignment.params.empty())
    {
        outfile << "Nil::NIL()";
    }
    else
    {
        emit_expr(assignment.params[0]);
    }
    outfile << ", " << local <<");\n";
}

void Generator::emit_return(ASTNode ret)
{
    outfile << "return ";
    if(!ret.params.empty() && ret.params.begin()->type != ASTNodeType::empty)
        emit_expr(*ret.params.begin());
    else
        outfile << "Nil::NIL()";
    outfile << ";";
}

void Generator::emit_forloop(ASTNode loop)
{
    auto init = loop.params[0];

    std::string var = init.params[0].token.raw;
    auto from = init.params[1];
    auto to = init.params[2];
    outfile << "{";
    emit_scope();
    outfile << "std::string var = \"" << var << "\";\n";
    outfile << "number_t start = "; emit_expr(from); outfile << "->to_number();\n";
    outfile << "number_t end = "; emit_expr(to); outfile << "->to_number();\n";
    outfile << "number_t step = ";
    if(init.params.size() == 4)
    {
        emit_expr(init.params[3]);
        outfile << "->to_number();\n";
    }
    else
    {
        outfile << "1;\n";
    }
    outfile << "if(start != end && !(start > end && step > 0) && !(start < end && step < 0)){\n";
    outfile << "for(; start != end; start += step){\n";
    outfile << "scope.set(var, Number::create(start), true);\n";
    for(auto statement = loop.params.begin() + 1; statement != loop.params.end(); statement++)
    {
        emit_statement(*statement);
    }
    outfile << "}";
    outfile << "scope.set(var, Number::create(start), true);\n";
    for(auto statement = loop.params.begin() + 1; statement != loop.params.end(); statement++)
    {
        emit_statement(*statement);
    }
    outfile << "}";
    outfile << "}";
}

void Generator::emit_condition(ASTNode condition)
{
    for(auto part: condition.params)
    {
        std::string statement = part.token.raw;
        outfile << (statement == "elseif" ? "else if" : statement);
        if(statement != "else")
        {
            outfile << "(";
            emit_expr(part.params[0]);
            outfile << "->to_bool()";
            outfile << ")";
        }
        outfile << "{";
        emit_scope();
        for(auto stm = part.params.begin() + (statement != "else"); stm != part.params.end(); stm++)
        {
            emit_statement(*stm);
        }
        outfile << "}";
    }
}


void Generator::emit_expr(ASTNode expr)
{
    switch(expr.type)
    {
        case ASTNodeType::literal:
            emit_literal(expr); break;
        case ASTNodeType::id:
            emit_identifier(expr); break;
        case ASTNodeType::funcdef:
            emit_funcdef(expr); break;
        case ASTNodeType::funccall:
            emit_funccall(expr); break;
        case ASTNodeType::binop:
            emit_binop(expr); break;
        case ASTNodeType::unop:
            emit_unop(expr); break;
    }
}

void Generator::emit_literal(ASTNode literal)
{
    auto token = literal.token;
    if(token.type == LuaTokenType::nil)
    {
        outfile << "Nil::NIL()";
        return;
    }
    if(token.type == LuaTokenType::string)outfile << "String";
    if(token.type == LuaTokenType::number)outfile << "Number";
    if(token.type == LuaTokenType::boolean)outfile << "Boolean";
    outfile << "::create(" << literal.token.raw << ")";
}

void Generator::emit_identifier(ASTNode identifier)
{
    outfile << "scope.get(\"" << identifier.token.raw << "\")";
}

void Generator::emit_funcdef(ASTNode funcdef)
{
    outfile << "Function::create([&](auto p){";
    emit_scope();
    size_t param_idx = 0;
    for(auto arg = funcdef.params.begin(); arg->type == ASTNodeType::id; arg++)
    {
        outfile << "if(" << param_idx << " < p.size())";
        outfile << "scope.set(\"" << arg->token.raw << "\", p[" << param_idx++ << "], true);";
    }
    auto funcbody = *funcdef.params.rbegin();
    assert(funcbody.type == ASTNodeType::funcbody);
    for(auto statement: funcbody.params)
    {
        emit_statement(statement);
    }
    if(funcbody.params.rbegin()->type != ASTNodeType::ret)
        outfile << "return Nil::NIL();";
    outfile << "})";
}

void Generator::emit_binop(ASTNode operation)
{
    auto lhs = operation.params[0];
    auto rhs = operation.params[1];
    auto oper = operation.token.raw;
    if(oper == "..") // special cases - incompatibile operators
    {
        outfile << "String::create(";
        emit_expr(lhs);
        outfile << "->to_string() + ";
        emit_expr(rhs);
        outfile << "->to_string())";
        return;
    }
    if(oper == "and")
    {
        outfile << "(";
        emit_expr(lhs);
        outfile << "->to_bool() ? ";
        emit_expr(rhs);
        outfile << " : ";
        emit_expr(lhs);
        outfile << ")";
        return;
    }
    if(oper == "or")
    {
        outfile << "(";
        emit_expr(lhs);
        outfile << "->to_bool() ? ";
        emit_expr(lhs);
        outfile << " : ";
        emit_expr(rhs);
        outfile << ")";
        return;
    }
    if(oper == "~=")oper = "!=";

    outfile << "(";
    emit_expr(lhs);
    outfile << oper;
    emit_expr(rhs);
    outfile << ")";
}

void Generator::emit_unop(ASTNode operation)
{
    auto operand = operation.params[0];
    auto oper = operation.token.raw;
    if(oper == "#")
    {
        outfile << "len(";
        emit_expr(operand);
        outfile << ")";
        return;
    }
    if(oper == "not")
    {
        outfile << "(!";
        emit_expr(operand);
        outfile << ")";
        return;
    }
    if(oper == "-")
    {
        outfile << "(-";
        emit_expr(operand);
        outfile << ")";
        return;
    }
}


}}



#endif
