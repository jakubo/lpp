#ifndef LPP_PATTERNS_HPP__
#define LPP_PATTERNS_HPP__


namespace lpp{ namespace parse{

const auto NIL_PATTERN = std::regex(R"(^nil$)");
const auto BOOL_PATTERN = std::regex(R"(^(true|false)$)");
const auto NUM_PATTERN = std::regex(R"(^((0x[\da-fA-F]+)|((\d*\.?\d+)(e\-?\d+)?))$)");
const auto STR_PATTERN = std::regex(R"(^"(([^"])|(\\"))*"$)");
const auto STR_OPEN_PATTERN = std::regex(R"(^"(([^"])|(\\"))*$)");
const auto IDENTIFIER_PATTERN = std::regex(R"(^[a-zA-Z_]+[a-zA-Z0-9_]*$)");
const auto WHITESPACE_PATTERN = std::regex(R"(^\s+$)");

}}

#endif
