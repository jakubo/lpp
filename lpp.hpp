#ifndef LPP_LPP_HPP__
#define LPP_LPP_HPP__

#include<map>
#include<deque>
#include<cmath>
#include<string>
#include<memory>
#include<limits>
#include<vector>
#include<sstream>
#include<iostream>
#include<functional>

namespace lpp{

class Type;
class Scope;

typedef long double number_t;
typedef long long number_int_t;
typedef std::shared_ptr<Type> VarType;
typedef std::shared_ptr<Scope> ScopePtr;
typedef std::map<std::string, VarType> VarReg;
typedef std::deque<VarReg> Context;
typedef std::function<VarType(std::vector<VarType>)> function_t;
typedef std::function<number_t(number_t, number_t)> operation_t;
typedef std::function<void()> for_t;

const number_t INVALID_NUM = -std::numeric_limits<number_t>::infinity();


enum class LuaType{ string, number, function, boolean, nil };

 
class Type
{
    public:
        Type(LuaType t=LuaType::nil): type_{t}, nil{false} {}
        virtual std::string to_string() const = 0;
        virtual number_t to_number() const = 0;
        virtual bool to_bool() const { return not nil; }

        LuaType type()const { return nil ? LuaType::nil : type_; }
        std::string type_name() const
        {
            switch(type_){
                case(LuaType::string): return "string";
                case(LuaType::number): return "number";
                case(LuaType::boolean): return "boolean";
                case(LuaType::function): return "function";
                default: return "nil";
            }
        }

        operator std::string() const { return to_string(); }
        operator number_t() const { return to_number(); }
        operator bool() const { return to_bool(); }

        operator number_int_t() const { return static_cast<number_int_t>(to_number()); }

        VarType call(std::vector<VarType> params)
        {
            if(type_ != LuaType::function)
                throw std::runtime_error("Attempt to call " + type_name());
        }

        ~Type() = default;

    protected:
        LuaType type_;
        bool nil;
};


std::ostream& operator << (std::ostream& os, Type& tp)
{
    return os << tp.to_string();
}

std::ostream& operator << (std::ostream& os, VarType vt)
{
    return os << vt->to_string();
}


class Number: public Type
{
    public:
        static VarType create(number_t n)
        {
            return VarType(new Number(n));
        }

        std::string to_string() const override
        {
            if(nil) return "nil";
            return std::to_string(val);
        }
        number_t to_number() const override { return nil ? INVALID_NUM : val; }


    private:
        number_t val;
        Number(number_t n): Type{LuaType::number}, val{n} {}
};

class String: public Type
{
    public:
        static VarType create(std::string s)
        {
            return VarType(new String(s));
        }

        std::string to_string() const override { return nil ? "nil" : val; }
        number_t to_number() const override
        {
            try{ return std::stold(val); }
            catch(std::invalid_argument){ return INVALID_NUM; }
        }

    private:
        std::string val;
        String(std::string s): Type{LuaType::string},  val{s} {}

};

class Function: public Type
{
    public:

        static VarType create(function_t f)
        {
            return VarType(new Function(f));
        }
        std::string to_string() const override
        {
            std::ostringstream s;
            s << "function: " << this;
            return s.str();
        }
        number_t to_number() const override
        {
            return INVALID_NUM;
        }
        VarType call(std::vector<VarType> params)
        {
            return fun(params);
        }


    private:
        function_t fun;
        Function(function_t f): Type{LuaType::function}, fun{f} {}
};


class Boolean: public Type
{
    public:
        static VarType TRUE()
        {
            static VarType true_;
            if(true_){ return true_; }
            true_ = VarType(new Boolean(true));
            return true_;
        }
        static VarType FALSE()
        {
            static VarType false_;
            if(false_){ return false_; }
            false_ = VarType(new Boolean(false));
            return false_;
        }
        static VarType create(bool v){ return v? Boolean::TRUE() : Boolean::FALSE(); }

        std::string to_string() const override
        {
            return nil ? "nil" : (val ? "true" : "false");
        }
        number_t to_number() const override { return INVALID_NUM; }
        bool to_bool() const override { return !nil && val; }

        bool val;
    private:
        Boolean(bool b): Type{LuaType::boolean}, val{b} {}
};


class Nil: public Type
{
    public:
        static VarType NIL()
        {
            static VarType nil_;
            if(nil_){ return nil_;}
            return VarType(new Nil());
        }
        std::string to_string() const override { return "nil"; }
        number_t to_number() const override { return INVALID_NUM; }
        bool to_bool() const override { return false; }
    private:
        Nil(LuaType t=LuaType::nil){nil=true;}
};

VarType operator!(VarType b)
{
    return Boolean::create(!b->to_bool());
}
VarType operator-(VarType n)
{
    if(n->type() != LuaType::number)
        throw std::runtime_error("Attempt to perform arithmetic on " + n->type_name());
    return Number::create(-n->to_number());
}

VarType operator==(VarType l, VarType r)
{
    if(l->type() != r->type()) return Boolean::FALSE();
    if(l->type() == LuaType::nil) return Boolean::TRUE();
    if(l->type() == LuaType::number) return Boolean::create(l->to_number() == r->to_number());
    if(l->type() == LuaType::string) return Boolean::create(l->to_string() == r->to_string());
    if(l->type() == LuaType::boolean) return Boolean::create(l->to_bool() == r->to_bool());
    if(l->type() == LuaType::function) return Boolean::create(&(*l) == &(*r));
    return Boolean::FALSE();
}

VarType operator!=(VarType l, VarType r){ return !(l == r); }


VarType operator>(VarType l, VarType r)
{
    if(l->type() != r->type() || l->type() == LuaType::boolean || l->type() == LuaType::function)
        throw std::runtime_error("attempt to compare " + l->type_name() + " with " + r->type_name());
    if(l->type() == LuaType::nil) return Boolean::TRUE();
    if(l->type() == LuaType::number) return Boolean::create(l->to_number() > r->to_number());
    if(l->type() == LuaType::string) return Boolean::create(l->to_string() > r->to_string());
    return Boolean::FALSE();
}


VarType operator<(VarType l, VarType r){ return !Boolean::create(l == r || l > r); }
VarType operator>=(VarType l, VarType r) { return Boolean::create(l == r || l > r); }
VarType operator<=(VarType l, VarType r) { return Boolean::create(l == r || l < r); }


VarType arithmetic_operation(VarType l, VarType r, operation_t operation)
{
    auto num_l = l->to_number();
    auto num_r = r->to_number();
    if(num_l == INVALID_NUM || num_r == INVALID_NUM)
        throw std::runtime_error("Attempt to perform arithmetic on " + l->type_name() + " and " + r->type_name());
    return Number::create(operation(num_l, num_r));
}


VarType operator+(VarType lhs, VarType rhs)
{
    return arithmetic_operation(lhs, rhs, [](number_t l, number_t r){return l+r;});
}
VarType operator-(VarType lhs, VarType rhs)
{
    return arithmetic_operation(lhs, rhs, [](number_t l, number_t r){return l-r;});
}
VarType operator*(VarType lhs, VarType rhs)
{
    return arithmetic_operation(lhs, rhs, [](number_t l, number_t r){return l*r;});
}
VarType operator/(VarType lhs, VarType rhs)
{
    return arithmetic_operation(lhs, rhs, [](number_t l, number_t r){return l/r;});
}
VarType operator%(VarType lhs, VarType rhs)
{
    return arithmetic_operation(lhs, rhs, [](number_int_t l, number_int_t r){return l%r;});
}
VarType operator^(VarType lhs, VarType rhs)
{
    return arithmetic_operation(lhs, rhs, [](number_int_t l, number_int_t r){return std::pow(l, r);});
}


class Scope
{
    public:
        Scope() = delete;
        Scope(Scope&) = delete;
        Scope(Scope&&) = delete;
        Scope(Context& context)
        {
            cntx = &context;
            cntx->push_back(VarReg());
        }

        ~Scope(){ cntx->pop_back();}

        VarType get(std::string name)
        {
            for(auto i=cntx->rbegin(); i!=cntx->rend(); i++)
            {
                if((*i).find(name) != (*i).end()) return (*i)[name];
            }
            return Nil::NIL();
        }

        void set(std::string name, VarType val, bool force_local=false)
        {
            if(force_local)
            {
                cntx->back()[name] = val;
                return;
            }
            auto local = cntx->back();
            if(local.find(name) != local.end())
            {
                local[name] = val;
                return;
            }
            cntx->front()[name] = val;
        }
    private:
        Context *cntx;
};

void load_stdlib(Scope& global)
{
    auto print = Function::create([&](auto params){
        bool first = true;
        for(auto param: params)
        {
            if(!first)std::cout << '\t';
            first = false;
            std::cout << *param;
        }
        std::cout << '\n';
        return Nil::NIL();
    });
    global.set("print", print);
}

VarType len(VarType var)
{
    if(var->type() != LuaType::string)
        throw std::runtime_error("Attempt to get length of a " + var->type_name());
    return Number::create(var->to_string().size());
}



}
#endif
