#ifndef LPP_LEX_HPP__
#define LPP_LEX_HPP__

#include<regex>
#include<string>
#include<vector>
#include<fstream>
#include<sstream>
#include<algorithm>


#include "patterns.hpp"


namespace lpp{namespace lex{
using namespace parse;




const auto COMMENT_PATTERN = std::regex(R"(--.*$)");

enum class LuaTokenType
{
    id, number, nil, boolean, string, lparen, rparen, lcurly, rcurly, lsqr, rsqr,
    oper, assign, comma, dot, semicolon, comment, space, invalid, keyword
};


const std::vector<std::string> OPERATORS = {
    "+", "-", "*", "/", "^", "%", "<", ">", "<=",
    ">=", "==", "~=", "..", "and", "or"
};
const std::vector<std::string> UNOPS = {"not", "#", "-"};


const std::vector<std::string> keywords = {
    "break", "do", "else", "elseif", "end", "for", "function",
    "if", "in", "local", "repeat", "return", "then",
    "until", "while"
};


struct LuaToken
{
    LuaTokenType type;
    std::string raw;
    std::size_t row;
    std::size_t col;
    std::string type_name()
    {
        switch(type)
        {
            case(LuaTokenType::id): return "id";
            case(LuaTokenType::number): return "number";
            case(LuaTokenType::nil): return "nil";
            case(LuaTokenType::boolean): return "bool";
            case(LuaTokenType::string): return "string";
            case(LuaTokenType::lparen): return "left paren";
            case(LuaTokenType::rparen): return "right paren";
            case(LuaTokenType::lcurly): return "left curly brace";
            case(LuaTokenType::rcurly): return "right curly brace";
            case(LuaTokenType::lsqr): return "left square brace";
            case(LuaTokenType::rsqr): return "right square brace";
            case(LuaTokenType::oper): return "operator";
            case(LuaTokenType::assign): return "assignment";
            case(LuaTokenType::comma): return "comma";
            case(LuaTokenType::dot): return "dot";
            case(LuaTokenType::semicolon): return "semicolon";
            case(LuaTokenType::comment): return "comment";
            case(LuaTokenType::space): return "space";
            case(LuaTokenType::keyword): return "keyword";
            default: return "invalid";
        }
    }
};


LuaToken to_token(std::string str)
{
    LuaToken tok;
    tok.raw = str;
    tok.type = LuaTokenType::invalid;
    tok.row = 0;

    if(std::regex_match(str, IDENTIFIER_PATTERN))tok.type = LuaTokenType::id;
    if(std::find(keywords.begin(), keywords.end(), str) != keywords.end())tok.type = LuaTokenType::keyword;
    if(std::regex_match(str, NUM_PATTERN)) tok.type = LuaTokenType::number;
    if(std::regex_match(str, STR_PATTERN)) tok.type = LuaTokenType::string;
    if(std::regex_match(str, NIL_PATTERN)) tok.type = LuaTokenType::nil;
    if(std::regex_match(str, BOOL_PATTERN)) tok.type = LuaTokenType::boolean;
    if(str == "(") tok.type = LuaTokenType::lparen;
    if(str == ")") tok.type = LuaTokenType::rparen;
    if(str == "{") tok.type = LuaTokenType::lcurly;
    if(str == "}") tok.type = LuaTokenType::rcurly;
    if(str == "[") tok.type = LuaTokenType::lsqr;
    if(str == "]") tok.type = LuaTokenType::rsqr;
    if(std::find(OPERATORS.begin(), OPERATORS.end(), str) != OPERATORS.end()) tok.type = LuaTokenType::oper;
    if(std::find(UNOPS.begin(), UNOPS.end(), str) != UNOPS.end()) tok.type = LuaTokenType::oper;
    if(str == "=") tok.type = LuaTokenType::assign;
    if(str == ",") tok.type = LuaTokenType::comma;
    if(str == ".") tok.type = LuaTokenType::dot;
    if(str == ";") tok.type = LuaTokenType::semicolon;
    if(std::regex_match(str, COMMENT_PATTERN)) tok.type = LuaTokenType::comment;
    if(std::regex_match(str, WHITESPACE_PATTERN)) tok.type = LuaTokenType::space;
    
    return tok;
}


std::vector<LuaToken> tokenize(std::string line, std::size_t line_no)
{
    std::vector<LuaToken> tokens;
    std::size_t start = 0;
    std::string s = line + " ";
    for(auto i=s.begin(); i!=s.end(); i++)
    {
        for(auto r=s.rbegin(); (r.base() -1) != i; r++)
        {
            std::string so_far(i, r.base()-1);
            auto tok = to_token(so_far);
            if(tok.type != LuaTokenType::invalid)
            {
                tok.row = line_no;
                tok.col = start;
                if(tok.type != LuaTokenType::semicolon &&
                   tok.type != LuaTokenType::space &&
                   tok.type != LuaTokenType::comment)
                    tokens.push_back(tok);
                i += so_far.size() - 1;
                start += so_far.size() -1;
                break;
            }
        }
    }
    return tokens;
}


std::vector<LuaToken> tokenize(std::ifstream& source)
{
    std::vector<LuaToken> tokens;
    std::size_t lno = 0;
    std::string line;
    while(std::getline(source, line))
    {
        auto line_tokens = tokenize(line, lno++);
        tokens.insert(tokens.end(), line_tokens.begin(), line_tokens.end());
    }
    return tokens;
}




}}

#endif
