#include<vector>
#include<fstream>
#include<iostream>
#include<algorithm>

#include "lex.hpp"
#include "parse.hpp"
#include "lpp.hpp"
#include "codegen.hpp"


void show_lex_output(const std::vector<lpp::lex::LuaToken>& tokens);

int main(int argc, char* argv[])
{
    std::vector<std::string>args(argv+1, argv+argc);
    bool show_lex = false, show_parse = false, show_program = false;
    auto lex = std::find(args.begin(), args.end(), "--lex");
    if(lex != args.end())
    {
        show_lex = true;
        args.erase(lex);
    }
    auto parse = std::find(args.begin(), args.end(), "--parse");
    if(parse != args.end())
    {
        show_parse = true;
        args.erase(parse);
    }
    auto program = std::find(args.begin(), args.end(), "--generate");
    if(program != args.end())
    {
        show_program = true;
        args.erase(program);
    }
    std::ifstream source;
    std::string output = args[0].substr(0, args[0].find(".lua"));
    source.open(args[0]);
    auto tokens = lpp::lex::tokenize(source);
    if(show_lex)show_lex_output(tokens);

    lpp::parse::Parser p(tokens);
    try
    {
        auto ast = p.parse_program();
        if(show_parse) ast.show();
        lpp::codegen::Generator code(output + ".cpp");
        code.generate(ast);
    }catch(lpp::parse::parse_error& e)
    {
        std::cout << e.what() << std::endl;
    }


    return 0;
}


void show_lex_output(const std::vector<lpp::lex::LuaToken>& tokens)
{
    for(auto token: tokens)
    {
        std::cout << token.row << ", " << token.col
            << '\t' << token.raw << '\t' << token.type_name()
            << std::endl;
    }
}
