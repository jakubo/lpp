fib = function(nth)
    if nth == 0 then return 0 end
    if nth == 1 then return 1 end
    return fib(nth-1) + fib(nth-2)
end


for i=1,35 do
    print("Term#"..i.." = "..fib(i))
end
